﻿using Store.Core.Interfaces;
using Store.Core.Repositories;

namespace Store.Core
{
    public class UnitOfWork : IUnitOfWork
    {
        private IOrderRepository _orderRepository { get; }
        private IProductRepository _productRepository { get; }
        public UnitOfWork(string connectionString)
        {
            _orderRepository = new OrderRepository(connectionString);
            _productRepository = new ProductRepository(connectionString);
        }
        public IOrderRepository OrderRepository { get { return _orderRepository; } }
        public IProductRepository ProductRepository { get { return _productRepository; } }
    }
}
