﻿using Store.Core.Entities;
using Store.Core.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Store.Core.Interfaces
{
    public interface IOrderRepository : IRepository<Order>
    {
        Task<IEnumerable<Order>> GetAllOrdersByFilterAsync(int? month = null, OrderStatus? status = null, int? year = null, int? productId = null);
        Task BulkDeleteOrdersAsync(int? month = null, OrderStatus? status = null, int? year = null, int? productId = null);

    }
}
