﻿using System.Data.SqlClient;

namespace Store.Core.Interfaces
{
    public interface IMapper<TResult>
    {
        TResult Map(SqlDataReader data);
    }
}
