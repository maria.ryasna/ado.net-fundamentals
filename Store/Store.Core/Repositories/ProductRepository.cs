﻿using Store.Core.Entities;
using Store.Core.Interfaces;
using Store.Core.Mappers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Store.Core.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly string _connectionString;
        private readonly IMapper<Product> _mapper;
        public ProductRepository(string connectionString)
        {
            _connectionString = connectionString;
            _mapper = new ProductMapper();
        }
        public async Task CreateAsync(Product product)
        {
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                string query = $"SET IDENTITY_INSERT [dbo].[Product] ON; INSERT INTO[dbo].[Product] ([Id],[Name],[Description],[Weight],[Height],[Width],[Length])  VALUES({product.Id},'{product.Name}','{product.Description}',{product.Weight},{product.Height},{product.Width},{product.Length}); SET IDENTITY_INSERT [dbo].[Product] OFF;";
                SqlCommand command = new SqlCommand(query, connection);
                command.ExecuteNonQuery();
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                string query = $"DELETE FROM [dbo].[Product] WHERE Id = {id}";
                SqlCommand command = new SqlCommand(query, connection);
                command.ExecuteNonQuery();
            }
        }

        public async Task<IEnumerable<Product>> GetAllProductsAsync()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                string query = "SELECT * FROM [dbo].[Product]";
                SqlCommand command = new SqlCommand(query, connection);

                SqlDataReader data = command.ExecuteReader();

                var products = new List<Product>();

                if (data.HasRows)
                {
                    while (data.Read())
                    {
                        var product = _mapper.Map(data);
                        products.Add(product);
                    }
                }

                return products;
            }
        }

        public async Task<Product> ReadAsync(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                string query = $"SELECT * FROM [dbo].[Product] WHERE Id = {id}";
                SqlCommand command = new SqlCommand(query, connection);
                SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet);

                foreach (DataTable table in dataSet.Tables)
                {
                    foreach (DataRow row in table.Rows)
                    {
                        return new Product
                        {
                            Id = (int)row[0],
                            Name = row[1].ToString(),
                            Description = row[2].ToString(),
                            Weight = (double)row[3],
                            Height = (double)row[4],
                            Width = (double)row[5],
                            Length = (double)row[6],

                        };
                    }
                }

                return null;
            }
        }

        public async Task UpdateAsync(Product product)
        {
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                string query = @$"UPDATE [dbo].[Product] SET 
                        Name = '{product.Name}', 
                        Description = '{product.Description}',
                        Weight = '{product.Weight}', 
                        Height = '{product.Height}', 
                        Width = '{product.Width}', 
                        Length = '{product.Length}'
                        WHERE Id = {product.Id}";
                SqlCommand command = new SqlCommand(query, connection);
                command.ExecuteNonQuery();
            }
        }
    }
}
