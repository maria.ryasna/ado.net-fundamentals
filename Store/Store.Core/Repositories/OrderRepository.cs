﻿using Store.Core.Entities;
using Store.Core.Enums;
using Store.Core.Interfaces;
using Store.Core.Mappers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Store.Core.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly string _connectionString;
        private readonly IMapper<Order> _mapper;
        private readonly string dateTimeFormat = "MM/dd/yyyy HH:mm:ss";
        public OrderRepository(string connectionString)
        {
            _connectionString = connectionString;
            _mapper = new OrderMapper();
        }

        public async Task BulkDeleteOrdersAsync(int? month = null, OrderStatus? status = null, int? year = null, int? productId = null)
        {
            SqlConnection connection = new SqlConnection(_connectionString);
            await connection.OpenAsync();
            SqlTransaction transaction = connection.BeginTransaction();
            var filteredOrders = await GetAllOrdersByFilterAsync(month, status, year, productId);

            foreach (var order in filteredOrders)
            {
                try
                {
                    string query = $"DELETE FROM [dbo].[Order] WHERE Id = {order.Id}";
                    SqlCommand command = new SqlCommand(query, connection);
                    command.ExecuteNonQuery();
                    await transaction.CommitAsync();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                }
            }
        }

        public async Task CreateAsync(Order order)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                string query = $"SET IDENTITY_INSERT [dbo].[Order] ON; INSERT INTO [dbo].[Order] ([Id],[Status],[CreatedDate],[UpdatedDate],[ProductId]) VALUES ({order.Id},{(int)order.Status},'{DateTime.Now.ToString(dateTimeFormat)}','{DateTime.Now.ToString(dateTimeFormat)}',{order.ProductId}); SET IDENTITY_INSERT [dbo].[Order] OFF;";
                SqlCommand command = new SqlCommand(query, connection);
                command.ExecuteNonQuery();
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                string query = $"DELETE FROM [dbo].[Order] WHERE Id = {id}";
                SqlCommand command = new SqlCommand(query, connection);
                command.ExecuteNonQuery();
            }
        }

        public async Task<IEnumerable<Order>> GetAllOrdersByFilterAsync(int? month = null, OrderStatus? status = null, int? year = null, int? productId = null)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                string procedure = "spOrder_GetAllOrdersByFilter";
                SqlCommand command = new SqlCommand(procedure, connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("@Month", month);
                command.Parameters.AddWithValue("@Year", year);
                command.Parameters.AddWithValue("@ProductId", productId);

                if (status != null)
                {
                    command.Parameters.AddWithValue("@Status", (int)status);
                }

                await connection.OpenAsync();

                SqlDataReader data = command.ExecuteReader();

                var orders = new List<Order>();

                if (data.HasRows)
                {
                    while (data.Read())
                    {
                        var order = _mapper.Map(data);
                        orders.Add(order);
                    }
                }

                return orders;
            }
        }

        public async Task<Order> ReadAsync(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                string query = $"SELECT * FROM [dbo].[Order] WHERE Id = {id}";
                SqlCommand command = new SqlCommand(query, connection);
                SqlDataReader data = command.ExecuteReader();

                if (data.HasRows)
                {
                    data.Read();
                    return _mapper.Map(data);
                }

                return null;
            }
        }

        public async Task UpdateAsync(Order order)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                string query = @$"UPDATE [dbo].[Order] SET 
                        Status = '{(int)order.Status}', 
                        UpdatedDate = '{DateTime.Now.ToString(dateTimeFormat)}',
                        ProductId = '{order.ProductId}'
                        WHERE Id = {order.Id}";
                SqlCommand command = new SqlCommand(query, connection);
                command.ExecuteNonQuery();
            }
        }
    }
}
