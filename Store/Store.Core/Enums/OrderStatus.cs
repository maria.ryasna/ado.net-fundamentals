﻿namespace Store.Core.Enums
{
    public enum OrderStatus
    {
        NotStarted = 0, 
        Loading, 
        InProgress, 
        Arrived, 
        Unloading, 
        Cancelled, 
        Done
    }
}
