﻿using Store.Core.Entities;
using Store.Core.Enums;
using Store.Core.Interfaces;
using System;
using System.Data.SqlClient;

namespace Store.Core.Mappers
{
    public class OrderMapper : IMapper<Order>
    {
        public Order Map(SqlDataReader data)
        {
            return new Order
            {
                Id = (int)data.GetValue(0),
                Status = (OrderStatus)data.GetValue(1),
                CreatedDate = (DateTime)data.GetValue(2),
                UpdatedDate = (DateTime)data.GetValue(3),
                ProductId = (int)data.GetValue(4)
            };
        }
    }
}
