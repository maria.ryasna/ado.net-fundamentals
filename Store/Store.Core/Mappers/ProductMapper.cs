﻿using Store.Core.Entities;
using Store.Core.Interfaces;
using System.Data.SqlClient;

namespace Store.Core.Mappers
{
    public class ProductMapper : IMapper<Product>
    {
        public Product Map(SqlDataReader data)
        {
            return new Product
                {
                    Id = (int)data.GetValue(0),
                    Name = data.GetValue(1).ToString(),
                    Description = data.GetValue(2).ToString(),
                    Weight = (double)data.GetValue(3),
                    Height = (double)data.GetValue(4),
                    Width = (double)data.GetValue(5),
                    Length = (double)data.GetValue(6)
                };
        }
    }
}
